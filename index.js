const electron = require('electron'); 
const path = require('path'); 
const fs = require('fs');
const axios = require('axios');
const FormData = require('form-data');
const accessToken = "8a27d750b3cdb061967b2e9c9f6d45378b2a40bf";
const folderId = "NDNfMTQ3MDE4Nl9vQmlOWA";
const sessionId = "OAUTH";

const { ClientRequest } = require('http');
  
// Importing dialog module using remote 
const dialog = electron.remote.dialog; 
var net= electron.remote.net  
var uploadFile = document.getElementById('upload'); 
  
// Defining a Global file path Variable to store  
// user-selected file 
global.filepath = undefined; 
global.fileBytes = undefined;
global.fileBlob = undefined;
uploadFile.addEventListener('click', () => {
// If the platform is 'win32' or 'Linux' 
    if (process.platform !== 'darwin') { 
        // // Resolves to a Promise<Object> 
        // dialog.showOpenDialog({ 
        //     title: 'Select the File to be uploaded', 
        //     defaultPath: path.join(__dirname, './assets/'), 
        //     buttonLabel: 'Upload',  
        //     properties: ['openFile'] 
        // }).then(file => { 
        //     // Stating whether dialog operation was 
        //     // cancelled or not. 
        //     console.log(file.canceled); 
        //     if (!file.canceled) { 
        //       // Updating the GLOBAL filepath variable  
        //       // to user-selected file. 
        //       global.filepath = file.filePaths[0].toString();
        //       uploadToOD();
        //     }   
        // }).catch(err => { 
        //     console.log(err) 
        // }); 

        function errorCallback(e) {
            console.log('Error', e)
        }

        navigator.getUserMedia({video: true, audio: true}, (localMediaStream) => {
            // var video = document.querySelector('video')
            // video.srcObject = localMediaStream;
            // console.log(localMediaStream);
            // video.play();
            // video.onloadedmetadata = (e) => {
            //    // Ready to go. Do some stuff.
            // };

            var recorder = new MediaRecorder(localMediaStream);

            recorder.addEventListener('dataavailable', function(e) {
                // e.data contains the audio data! let's associate it to an <audio> element
                // var el = document.querySelector('video');
                // console.log(e.data);
                // el.src = e.data;
                global.filepath = e.data.stream();
                global.fileBlob = e.data;
                console.log(e.data);
                global.fileBytes = e.data.size;
                // uploadToOD();

            });

            // start recording here...
            recorder.start();

            setTimeout(() => {
                recorder.stop();
            }, 5000);
         }, errorCallback)

        // global.filepath = file.filePaths[0].toString();
        // uploadToOD();
    } 
    else { 
        // // If the platform is 'darwin' (macOS) 
        // dialog.showOpenDialog({ 
        //     title: 'Select the File to be uploaded', 
        //     defaultPath: path.join(__dirname, './assets/'), 
        //     buttonLabel: 'Upload',
        //     // Specifying the File Selector and Directory  
        //     // Selector Property In macOS 
        //     properties: ['openFile', 'openDirectory'] 
        // }).then(file => { 
        //     console.log(file.canceled); 
        //     if (!file.canceled) { 
        //       global.filepath = file.filePaths[0].toString();
        //       uploadToOD();
        //     }   
        // }).catch(err => { 
        //     console.log(err) 
        // }); 
    } 
}); 

uploadToOD = async () => {
    // const file = fs.createReadStream(global.filepath);
    const file = global.filePath;
    const fileName = new Date().getTime() + '.mp4';
    const fileBlob = new File([global.fileBlob], fileName);
    console.log(fileBlob);

    try {
        const newFile = await createFile(fileName);
        await uploadChunks(fileBlob, newFile.data.FileId, global.fileBytes, newFile.data.TempLocation);
    } catch (e) {
        console.log('errror', e);
    }
}


createFile = (fileName) => {

    var config = {
        method: 'post',
        url: 'https://dev.opendrive.com/api/v1/upload/create_file.json?access_token=' + accessToken,
        headers: { 
            'Content-Type': 'application/json'
        },
        data : {
            'session_id': sessionId,
            'folder_id': folderId,
            'file_name': fileName
        }
    };

    return axios(config);

}

uploadChunks = (file, fileId, fileSize, tempLocation) => {


    var form = new FormData();
    form.append('session_id', sessionId);
    form.append('file_id', fileId);
    form.append('chunk_offset', '0');
    form.append('chunk_size', fileSize);
    form.append('temp_location', tempLocation);
    form.append('file_data', file);

    var request = net.request({
        method: 'post',
        url: 'https://dev.opendrive.com/api/v1/upload/upload_file_chunk.json?access_token=' + accessToken,
        headers: form.getHeaders()
    });
    request.writable = true;
    form.pipe(request);

    request.on('response', function(res) {
        console.log(res.statusCode);
        closeUpload(fileId, fileSize, tempLocation);
    });

}

closeUpload = (fileId, fileSize, tempLocation) => {

    var config = {
        method: 'post',
        url: 'https://dev.opendrive.com/api/v1/upload/close_file_upload.json?access_token=' + accessToken,
        headers: { 
            'Content-Type': 'application/json'
        },
        data : {
            'session_id': sessionId,
            'file_id': fileId,
            'file_size': fileSize,
            'temp_location': tempLocation
        }
    };

    return axios(config);
}